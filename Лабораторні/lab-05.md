# ЛР 05. Створення нового Git-репозиторію


## Перелік
1. [Реєстрація на Github та створення репозиторію](lab-01.md)
2. [Встановлення та налаштування клієнту СКВ Git](lab-02.md)
3. [Клонування репозиторію засобами Git for Windows](lab-03.md)
4. [Додавання файлв у репозиторій на Github ](lab-04.md)
5. [Внесення змін до репозиторію](lab-05.md)
6. [Створення репозиторію для власних лабораторних робіт](lab-06.md)
7. [Командна робота над проектом на Github ](lab-07.md)
8. [Створення та видалення тегів](lab-08.md)
9. [Робота з історією змін та отримання старих версій](lab-09.md)
10. [Відміна індексованих змін та фіксацій](lab-10.md)
11. [Створення гілок та навігація у розгалуженному проекті](lab-11.md)
12. [Зміни у гілці master. Злиття гілок](lab-12.md)
13. [Штучне створення конфлікту та його вирішення](lab-13.md)
14. [Робота з графічним клієнтом TortoiseGit](lab-14.md)
15. [Робота з графічним клієнтом Github](lab-15.md)
16. [Встановлення та налаштування клієнту СКВ Subversion](lab-16.md)
17. [Робота з репозиторієм в консольному режимі TortoiseSVN](lab-17.md)
18. [Робота з версіями в TortoiseSVN](lab-18.md)
19. [Робота з репозиторієм в IDE Netbeans](lab-19.md)
20. [Створення репозиторію для дипломного проекту](lab-20.md)

## Мета роботи

Отримати навички внесення змін до локальної робочої копії та репозиторію на Github, навчитися працювати з файлом `.gitignore`

## Обладнання

Персональний комп'ютер. Текстовий редактор Sublime Text 3 або Notepad++. Web-браузер Chrome, Firefox, Opera, Internet Explorer, Git for Windows

## Теоретичні відомості

`.gitignore` потрібен для приховування файлів і папок від системи контролю версій Git. Зазвичай приховують конфігураційні файли (особливо з паролями), тимчасові файли і папки. `.gitignore` використовує glob формат для вибірки файлів.

Основний синтаксис `.gitignore`

*   Кожен рядок - окремий шаблон
*   Порожні рядки ігноруються
*   Рядки, що починаються з # є коментарями
*   Символ слеша "/" на початку рядку вказує на поточний каталог репозиторію (де розташований .gitignore)
*   Зірочка (\*) заміює будь-яку кількість символів в імені файлу
*   Дві зірочки (\*\*) використовуються для зазначення всіх підкаталогів від поточного
*   Знак оклику (!) на початку рядку інвертує шаблон (використовується для виключення вказаного)
*   Для екранування спеціальних символів використовується зворотній слеш "\\". Для ігнорування всього каталогу шаблон має закінчуватися на слеш (/), інакше правило буде вважатися іменем файлу.

Приклад файла `.gitignore`

```gitignore 
\# ігнорувати файл foo.txt.
foo.txt
\# ігнорувати всі html-файли
\*.html
\# але не ігнорувати foo.html
!foo.html
\# ігнорувати rar-файли в поточному каталозі
\# файл /temp/main.rar не буде проігноровано, т.я. він не в поточному каталозі
/\*.rar
\# ігнорувати css-файли з папки bar не включаючи підкаталоги
\# файл /bar/temp/main.css не буде проігноровано
/bar/\*.css
\# ігнорувати js-файли з папки bar и подпапок, якщо вони існують 
/bar/\*\*.\*.js	
```
		

## Хід роботи

1.  Перевірити глобальні налаштування Git for Windows
2.  Створити порожній каталог для нового репозиторію
3.  За допомогою команди `git init` ініціалізувати репозиторій
```bash
$ mkdir izvp_lab_my_lastname
$ cd izvp_lab_my_lastname
$ git init	
```
4.  Якщо репозиторій проініціалізовано правильно, в середені каталогу з'явиться підкаталог `.git`
5.  Помістити в каталог файли, та за допомогою консолі перевірити статус репозиторію за допомогою команди `git status`
6.  Додати до індексу нові файли
7.  Зафіксувати зміни в репозиторії
8.  Помістити в каталог файли через інтерфейс операційної системи або за допомогою файлового менеджера, та за допомогою консолі перевірити статус репозиторію
9.  Додати нові файли до переліку ігнорування
10.  Додати кілька підкаталогів, розмістити там файли з розширенням `*.css`, `*.js` та `*.html`
11.  Додати файли `*.html` до переліку ігнорування
12.  Додати до індексу нові файли
13.  Перевірити статус репозиторію
14.  За допомогою команди `git remote` під'єднати віддалений репозиторій заздалегіть створений на GitHub
```bash
$ git remote add pb https://github.com/your_name/your_repo		
```
15.  Підєднати ще один віддалений репозиторій на Bitbucket
16.  Вивантажити файли локального репозиторію на Github та Bitbucket
17.  Впевнитися за допомогою web-інтерфейсу, що завантаження пройшло корректно
18.  Впевнитися за допомогою web-інтерфейсу, що Файли з переліку ігнорування не вивантажилися у віддалений репозиторій
19.  Для кожного етапу роботи зробити знімки екрану або скопіювати текст консолі та додати їх у звіт з описом кожного скіншота
20.  Дати відповіді на контрольні запитання
21.  Зберегти звіт у форматі PDF

## Контрольні питання

1.  Де може бути розміщений файл `.gitignore`?
2.  Яким чином ігнорувати всі файли `*.php` окрім `index.php`?
3.  Які дії виконує шаблон `**`?
4.  Які дії виконує шаблон `!`?

## Довідники та додаткові матеріали


1.  [Specifies intentionally untracked files to ignore](https://git-scm.com/docs/gitignore)
2.  [О файле .gitignore](https://tyapk.ru/blog/post/gitignore)
3.  [Что скрывает от нас директория .git](https://habr.com/ru/post/143079/)
4.  [Git всередині: Каталог .git](https://githowto.com/uk/git_internals_git_directory)
5.  [Шаблони Git. Запис змін у репозиторій](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D1%8C-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9-%D0%B2-%D1%80%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B9)